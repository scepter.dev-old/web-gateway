const cors = require('cors');
const app = require('express')();
const port = process.env.PORT || 3000;
const routing = require("./routing");

const corsUtils = require('./corsUtils');
app.use(cors({origin: corsUtils.getOrigins()}));

app.use('/', routing);

app.listen(port, () => console.log(`Gateway started at port ${[port]}`));
