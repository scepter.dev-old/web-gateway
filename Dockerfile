FROM node:12.13.0-alpine

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY ./package*.json ./

RUN npm install --silent

COPY . /app

EXPOSE 3000

CMD ["node", "index"]
