const fs = require('fs');
const path = require('path');

const requiredServiceProperties = ['type', 'path'];

checkServiceJsonFormat = (file, service) => {
    requiredServiceProperties.forEach(prop => {
        if (!service.hasOwnProperty(prop)) {
            throw new Error(`${file} spec missing required property: ${prop}`);
        }
    });
};

checkUrlVariableExists = (endpoint) => {
    if (!process.env[endpoint.service]) {
        throw new Error(`${endpoint.service} url environment variable not found`);
    }
};

handleProxyType = (spec, endpoint) => {
    checkUrlVariableExists(endpoint);
    spec.proxy.push(endpoint);
};

handleRequestType = (spec, endpoint) => {
    spec.requests.push(endpoint);
};

module.exports = {
    readSpec: () => {
        let spec = { proxy: [], requests: [] };
        try {
            const directory = path.join(__dirname, 'endpoints');
            const files = fs.readdirSync(directory);
            files.forEach((file) => {
                const fileContents = fs.readFileSync(`${directory}/${file}`);
                const json = JSON.parse(fileContents);
                json.forEach(endpoint => {
                    checkServiceJsonFormat(file, endpoint);
                    switch(endpoint.type) {
                        case "proxy":
                            handleProxyType(spec, endpoint);
                            break;
                        case "request":
                            handleRequestType(spec, endpoint);
                            break;
                        default:
                            break;
                    }
                });

            });
        } catch (exception) {
            console.log(exception.message);
            process.exit(1);
        }
        return spec;
    },
};
