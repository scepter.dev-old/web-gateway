const express = require("express");
const router = express.Router();
const request = require("request-promise-native");
const proxy = require("express-http-proxy");
const admin = require("firebase-admin");

const proxyOptions = {
  proxyReqPathResolver: (req) => {
    const path = req.url.split("/");
    path.splice(0, 2);
    path.unshift("");
    return path.join("/");
  }
};

const Modes = {
  production: "production",
  development: "development",
  testing: "testing"
};

const mode = process.env.MODE || Modes.production;

const projectService = process.env.PROJECT_SERVICE || "http://localhost:3001";
const gitService = process.env.GIT_SERVICE || "http://localhost:7001";
const userService = process.env.USER_SERVICE || "http://localhost:3002";
const issueService = process.env.ISSUE_SERVICE || "http://localhost:3003";

var serviceAccount = require("./firebase.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://scepter-26dc3.firebaseio.com"
});

router.use(async (req, res, next) => {
  if (mode === Modes.production) {
    const authorizationHeader = req.headers["authorization"];
    if (authorizationHeader && authorizationHeader.split(" ").length >= 2 && authorizationHeader.split(" ")[0] === "Bearer") {
      try {
        const decoded = await admin.auth().verifyIdToken(req.headers["authorization"].split(" ")[1]);
        req.headers["user"] = decoded.sub;
        return next();
      } catch (error) {
        console.log(error);
      }
    }
  } else {
    const authorizationHeader = req.headers["authorization"];
    if (authorizationHeader && authorizationHeader.split(" ").length >= 2 && authorizationHeader.split(" ")[0] === "Bearer") {
      req.headers["user"] = 1;
      return next();
    }
  }
  console.log("unauthorized");
  return res.status(401).json();
});

router.get("/projects", async (req, res) => {
  const response = res;

  await request({
    uri: `${projectService}/projects`,
    qs: req.query,
    headers: req.headers,
  }).then(async projectsRes => {
      const projects = JSON.parse(projectsRes);
      projects.forEach(project => {
          project.issues = [];
      });

      await request({
          uri: `${issueService}/issues?project[]=${projects.map(project => project.id).join("&project[]=")}`,
          headers: req.headers,
          defaults: { simple: true }
      }).then(issuesRes => {
          const issues = JSON.parse(issuesRes);
          issues.forEach(issue => {
              projects.filter(project => project.id === issue.project).forEach(project => {
                  if (!project.issues) {
                      project.issues = [];
                  }
                  project.issues.push(issue);
              });
          });
          response.status(200).json(projects);
      }).catch(error => {
          console.log(error);
      });


  }).catch(error => {
    console.log(error);
  });
});

router.get("/projects/:id", async (req, res) => {
  const response = res;
  await request({
    uri: `${projectService}/projects/${req.params.id}`,
    qs: req.query,
    headers: req.headers,
    defaults: { simple: true }
  }).then(async projectRes => {
    const project = JSON.parse(projectRes);
    console.log(project);

    const issuesCall =  request({
      uri: `${issueService}/issues?project[]=${project.id}`,
      headers: req.headers,
      defaults: { simple: true }
    }).then(issuesRes => {
      return JSON.parse(issuesRes);
    }).catch(error => {
      console.log(error);
    });

    const userIds = [];

    userIds.push(project.owner);

    const usersCall = request({
      uri: `${userService}/users?ids[]=${userIds.join("&ids[]=")}`,
      headers: req.headers,
      defaults: { simple: true }
    }).then(usersRes => {
      return JSON.parse(usersRes);
    });


    Promise.all([issuesCall, usersCall]).then(values => {
      project.issues = values[0];
      const users = values[1];
      console.log(users);
      users.forEach(user => {
        project.members.forEach(member => {
          if(member.user === user.id) {
            member.user = user;
          }
        })

      });
      return response.status(200).json(project);
    })







  }).catch(error => {
    console.log(error);
  })
});

router.all("/projects*", proxy(`${projectService}/projects`));

router.all("/repositories*", proxy(`${projectService}/repositories`));

router.get("/git/*", proxy(`${gitService}`, proxyOptions));

router.all("/users*", proxy(`${userService}/users`));

router.get("/issues", async (req, res) => {
  const uri = `${issueService}/issues`;
  const response = res;
  request({ uri: uri, qs: req.query, headers: req.headers, defaults: { simple: true } }).then(async res => {

    const issues = JSON.parse(res);
    if (issues.map(issue => issue.project).length > 0) {
      let projectsUrl = `${projectService}/projects?ids[]=${issues.map(issue => issue.project).join("&ids[]=")}`;
      const res = await request({ uri: projectsUrl, headers: req.headers, defaults: { simple: true } });
      const projects = JSON.parse(res);
      projects.forEach(project => {
        issues.filter(issue => issue["project"] === project.id).forEach(issue => {
          issue["project"] = project;
        });
      });
    }

    let userIds = [];

    issues.map(issue => issue.assignees.map(assignee => assignee.user)).forEach(test => {
      userIds = userIds.concat(test);
    });

    issues.forEach(issue => {
      if (!userIds.includes(issue.author)) {
        userIds.push(issue.author);
      }
    });

    if (userIds.length > 0) {
      let usersUrl = `${userService}/users?ids[]=${userIds.join("&ids[]=")}`;
      const res = await request({ uri: usersUrl, headers: req.headers, defaults: { simple: true } });
      const users = JSON.parse(res);
      users.forEach(user => {
        issues.forEach(issue => {
          if (issue.author === user.id) {
            issue.author = user;
          }

          issue.assignees.filter(assignee => assignee.user === user.id).forEach(assignee => {
            assignee.user = user;
          });
        });
      });
    }


    response.status(201).json(issues);
  });
});

router.all("/issues*", proxy(`${issueService}/issues`));

module.exports = router;
